Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 1996-2024, David Turner, Robert Wilhelm, and Werner Lemberg.
License: FTL

Files: builds/amiga/*
Copyright: 2005-2024, Werner Lemberg and Detlef Würkner.
License: FTL

Files: builds/amiga/src/*
Copyright: 1996-2024, David Turner, Robert Wilhelm, Werner Lemberg and Detlef Würkner.
License: FTL

Files: docs/* ft2demos/src/* ft2demos/src/ftinspect/glyphcomponents/* src/gzip/* src/gzip/patches/*
Copyright: 1996-2024 David Turner, Robert Wilhelm and Werner Lemberg
License: FTL

Files: builds/amiga/src/base/ftdebug.c
Copyright: 1996-2024, David Turner, Robert Wilhelm, Werner Lemberg, and Detlef Wuerkner.
License: FTL

Files: builds/cmake/FindHarfBuzz.cmake
Copyright: 2019, Sony Interactive Entertainment Inc.
 2012, Intel Corporation
License: BSD-3-clause

Files: builds/mac/ftmac.c
Copyright: 1996-2024, Just van Rossum, David Turner, Robert Wilhelm, and Werner Lemberg.
License: FTL

Files: builds/unix/aclocal.m4
Copyright: 1996-2024, Free Software Foundation, Inc.
License: (FSFULLR or GPL-2) with Libtool exception

Files: builds/unix/ax_compare_version.m4
Copyright: 2008, Tim Toolan <toolan@ele.uri.edu>
License: FSFAP

Files: builds/unix/ax_prog_python_version.m4
Copyright: 2009, Francesco Salvestrini <salvestrini@users.sourceforge.net>
License: FSFAP

Files: builds/unix/ax_pthread.m4
Copyright: 2019, Marc Stevens <marc.stevens@cwi.nl>
 2011, Daniel Richard G. <skunk@iSKUNK.ORG>
 2008, Steven G. Johnson <stevenj@alum.mit.edu>
License: GPL-3+ with Autoconf-2.0~Archive exception

Files: builds/unix/configure
Copyright: 1992-1996, 1998-2017, 2020-2023, Free Software Foundation
License: FSFUL

Files: builds/unix/install-sh
Copyright: 1994, X Consortium
License: X11

Files: builds/unix/ltmain.sh
Copyright: 1996-2019, 2021, 2022, Free Software Foundation, Inc.
License: (Expat or GPL-2+) with Libtool exception

Files: builds/unix/pkg.m4
Copyright: 2004, Scott James Remnant <scott@netsplit.com>.
License: GPL-2+ with Autoconf-data exception

Files: debian/*
Copyright: 2018-2024, Hugh McMaster
 1996-2019, Christoph Lameter, Anthony Fok, Steve Langasek, et al.
License: GPL-2+

Files: debian/tests/*
Copyright: 2020, Simon McVittie
 2020, Collabora Ltd.
License: Expat

Files: debian/tests/control
Copyright: 2018-2024, Hugh McMaster
 1996-2019, Christoph Lameter, Anthony Fok, Steve Langasek, et al.
License: GPL-2+

Files: docs/CHANGES
 docs/CUSTOMIZE
 docs/INSTALL
 docs/INSTALL.ANY
 docs/INSTALL.GNU
 docs/INSTALL.UNIX
 docs/INSTALL.VMS
 docs/TODO
 docs/VERSIONS.TXT
 docs/formats.txt
 docs/raster.txt
 docs/release
Copyright: 1996-2024, David Turner, Robert Wilhelm, and Werner Lemberg.
License: FTL

Files: docs/DOCGUIDE
Copyright: 2018-2024, Nikhil Ramakrishnan, David Turner, Robert Wilhelm, and Werner Lemberg.
License: FTL

Files: docs/FTL.TXT
Copyright: 1996-2024, David Turner, Robert Wilhelm, and Werner Lemberg
License: FTL

Files: docs/GPLv2.TXT
Copyright: 1989, 1991, Free Software Foundation, Inc.
License: GPL-2

Files: docs/INSTALL.CROSS
Copyright: 2006-2024, suzuki toshiya, David Turner, Robert Wilhelm, and Werner Lemberg.
License: FTL

Files: docs/oldlogs/*
Copyright: 1996-2024, David Turner, Robert Wilhelm, and Werner Lemberg.
License: FTL

Files: ft2demos/graph/beos/grbeos.cpp
Copyright: 2001-2024, Michael Pfeiffer
License: FTL

Files: ft2demos/graph/grswizzle.c
Copyright: no-info-found
License: public-domain

Files: ft2demos/graph/mac/grmac.c
Copyright: 1999-2024, Just van Rossum, Antoine Leca
License: FTL

Files: ft2demos/graph/win32/*
Copyright: 1999-2024, Antoine Leca, David Turner, Robert Wilhelm, and Werner Lemberg.
License: FTL

Files: ft2demos/graph/x11/*
Copyright: 1999-2024, Antoine Leca, David Turner, Robert Wilhelm, and Werner Lemberg.
License: FTL

Files: ft2demos/meson_options.txt
Copyright: 2022-2024, David Turner, Robert Wilhelm, Werner Lemberg, and Charlie Jiang.
License: FTL

Files: ft2demos/src/ftinspect/meson.build
Copyright: 1996-2024, David Turner, Robert Wilhelm, and Werner Lemberg.
License: FTL

Files: ft2demos/src/md5.c
 ft2demos/src/md5.h
Copyright: 2001, Alexander Peslyak and it is hereby released to the
License: public-domain

Files: ft2demos/src/mlgetopt.c
 ft2demos/src/mlgetopt.h
Copyright: no-info-found
License: public-domain

Files: ft2demos/src/rsvg-port.c
 ft2demos/src/rsvg-port.h
Copyright: 2022-2024, David Turner, Robert Wilhelm, Werner Lemberg, and Moazin Khatti.
License: FTL

Files: ft2docs/docs/tutorial/example5.cpp
Copyright: 2016-2018, Static Jobs LLC
License: Expat

Files: include/dlg/*
Copyright: 2019, nyorain
License: BSL-1.0

Files: include/freetype/ftbzip2.h
Copyright: 2010-2024, Joel Klinghed.
License: FTL

Files: include/freetype/ftcid.h
Copyright: 2007-2024, Dereg Clegg and Michael Toftdal.
License: FTL

Files: include/freetype/ftgxval.h
Copyright: 2004-2024, Masatake YAMATO, Redhat K.K
License: FTL

Files: include/freetype/ftmac.h
Copyright: 1996-2024, Just van Rossum, David Turner, Robert Wilhelm, and Werner Lemberg.
License: FTL

Files: include/freetype/internal/ftgloadr.h
 include/freetype/internal/ftmemory.h
Copyright: 1996-2024, David Turner, Robert Wilhelm, and Werner Lemberg
License: FTL

Files: include/freetype/internal/fthash.h
Copyright: 2001-2015, Francesco Zappa Nardelli
 2000, Computing Research Labs, New Mexico State University
License: Expat

Files: include/freetype/internal/ftmmtypes.h
Copyright: 2022-2024, David Turner, Robert Wilhelm, Werner Lemberg, George Williams, and
License: FTL

Files: include/freetype/internal/ftrfork.h
Copyright: 2004-2024, Masatake YAMATO and Redhat K.K.
License: FTL

Files: include/freetype/internal/services/svcid.h
Copyright: 2007-2024, Derek Clegg and Michael Toftdal.
License: FTL

Files: include/freetype/internal/services/svfntfmt.h
Copyright: 2003-2024, David Turner, Robert Wilhelm, and Werner Lemberg.
License: FTL or XFree86

Files: include/freetype/internal/services/svgxval.h
Copyright: 2004-2024, Masatake YAMATO, Red Hat K.K.
License: FTL

Files: include/freetype/internal/services/svmm.h
Copyright: 1996-2024, David Turner, Robert Wilhelm, Werner Lemberg, and Dominik RÃ¶ttsches.
License: FTL

Files: include/freetype/internal/services/svttcmap.h
Copyright: 2003-2024, Masatake YAMATO, Redhat K.K.
License: FTL

Files: include/freetype/internal/services/svttglyf.h
Copyright: 2005-2024, David Turner.
License: FTL

Files: include/freetype/internal/svginterface.h
Copyright: 2022-2024, David Turner, Robert Wilhelm, Werner Lemberg, and Moazin Khatti.
License: FTL

Files: include/freetype/otsvg.h
Copyright: 2022-2024, David Turner, Robert Wilhelm, Werner Lemberg, and Moazin Khatti.
License: FTL

Files: src/autofit/afindic.c
 src/autofit/afindic.h
Copyright: 2007-2024, Rahul Bhalerao <rahul.bhalerao@redhat.com>, <b.rahul.pm@gmail.com>.
License: FTL

Files: src/autofit/ft-hb.c
 src/autofit/ft-hb.h
Copyright: 2015, Google, Inc.
 2009, 2023, Red Hat, Inc.
License: Expat~old

Files: src/base/ftbase.h
Copyright: 2008-2024, David Turner, Robert Wilhelm, Werner Lemberg, and suzuki toshiya.
License: FTL

Files: src/base/ftcid.c
Copyright: 2007-2024, Derek Clegg and Michael Toftdal.
License: FTL

Files: src/base/fterrors.c
Copyright: 2018-2024, Armin Hasitzka, David Turner, Robert Wilhelm, and Werner Lemberg.
License: FTL

Files: src/base/ftgloadr.c
Copyright: 1996-2024, David Turner, Robert Wilhelm, and Werner Lemberg
License: FTL

Files: src/base/ftgxval.c
Copyright: 2004-2024, Masatake YAMATO, Redhat K.K
License: FTL

Files: src/base/fthash.c
Copyright: 2001-2015, Francesco Zappa Nardelli
 2000, Computing Research Labs, New Mexico State University
License: Expat

Files: src/base/ftmac.c
Copyright: 1996-2024, Just van Rossum, David Turner, Robert Wilhelm, and Werner Lemberg.
License: FTL

Files: src/base/ftpatent.c
Copyright: 2005-2024, David Turner.
License: FTL

Files: src/base/ftrfork.c
Copyright: 2004-2024, Masatake YAMATO and Redhat K.K.
License: FTL

Files: src/base/md5.c
 src/base/md5.h
Copyright: 2001, Alexander Peslyak and it is hereby released to the
License: public-domain

Files: src/bdf/*
Copyright: 2000-2014, Francesco Zappa Nardelli
License: Expat

Files: src/bdf/bdf.h
 src/bdf/bdflib.c
Copyright: 2001-2015, Francesco Zappa Nardelli
 2000, Computing Research Labs, New Mexico State University
License: Expat

Files: src/bzip2/*
Copyright: 2010-2024, Joel Klinghed.
License: FTL

Files: src/cff/cffdrivr.c
Copyright: 1996-2024, David Turner, Robert Wilhelm, Werner Lemberg, and Dominik RÃ¶ttsches.
License: FTL

Files: src/dlg/dlg.c
Copyright: 2019, nyorain
License: BSL-1.0

Files: src/gxvalid/*
Copyright: 2004-2024, suzuki toshiya, Masatake YAMATO, Red hat K.K.
License: FTL

Files: src/gxvalid/gxvalid.c
 src/gxvalid/gxvalid.h
 src/gxvalid/gxvbsln.c
 src/gxvalid/gxvcommn.c
 src/gxvalid/gxvcommn.h
 src/gxvalid/gxverror.h
 src/gxvalid/gxvfeat.c
 src/gxvalid/gxvfeat.h
 src/gxvalid/gxvjust.c
 src/gxvalid/gxvkern.c
 src/gxvalid/gxvlcar.c
 src/gxvalid/gxvmod.c
 src/gxvalid/gxvmod.h
 src/gxvalid/gxvmort.c
 src/gxvalid/gxvmort.h
 src/gxvalid/gxvmort0.c
 src/gxvalid/gxvmort1.c
 src/gxvalid/gxvmort2.c
 src/gxvalid/gxvmort4.c
 src/gxvalid/gxvmort5.c
 src/gxvalid/gxvmorx.c
 src/gxvalid/gxvmorx.h
 src/gxvalid/gxvmorx0.c
 src/gxvalid/gxvmorx1.c
 src/gxvalid/gxvmorx2.c
 src/gxvalid/gxvmorx4.c
 src/gxvalid/gxvmorx5.c
 src/gxvalid/gxvopbd.c
 src/gxvalid/gxvprop.c
 src/gxvalid/gxvtrak.c
 src/gxvalid/module.mk
 src/gxvalid/rules.mk
Copyright: 2004-2024, suzuki toshiya, Masatake YAMATO, Red Hat K.K.
License: FTL

Files: src/gxvalid/gxvfgen.c
Copyright: 2004-2024, Masatake YAMATO and Redhat K.K.
License: FTL

Files: src/gzip/ftgzip.c
 src/gzip/rules.mk
Copyright: 1996-2024, David Turner, Robert Wilhelm, and Werner Lemberg.
License: FTL

Files: src/gzip/zlib.h
Copyright: 1995-2023, Jean-loup Gailly and Mark Adler
License: Zlib

Files: src/lzw/*
Copyright: 2004-2024, Albert Chin-A-Young.
License: FTL

Files: src/lzw/ftzopen.c
 src/lzw/ftzopen.h
Copyright: 2005-2024, David Turner.
License: FTL

Files: src/pcf/*
Copyright: 2000-2014, Francesco Zappa Nardelli
License: Expat

Files: src/pcf/pcferror.h
Copyright: 1996-2024, David Turner, Robert Wilhelm, and Werner Lemberg.
License: FTL

Files: src/pcf/pcfutil.c
Copyright: 1990, 1994, 1998, The Open Group
License: Expat-Open-Group

Files: src/psaux/*
Copyright: 2006-2014, Adobe Systems Incorporated.
License: FTL

Files: src/psaux/afmparse.c
 src/psaux/afmparse.h
 src/psaux/cffdecode.c
 src/psaux/cffdecode.h
 src/psaux/module.mk
 src/psaux/psaux.c
 src/psaux/psauxerr.h
 src/psaux/psauxmod.c
 src/psaux/psauxmod.h
 src/psaux/psconv.c
 src/psaux/psconv.h
 src/psaux/psobjs.c
 src/psaux/psobjs.h
 src/psaux/rules.mk
 src/psaux/t1cmap.c
 src/psaux/t1cmap.h
 src/psaux/t1decode.c
 src/psaux/t1decode.h
Copyright: 1996-2024, David Turner, Robert Wilhelm, and Werner Lemberg.
License: FTL

Files: src/sfnt/pngshim.c
 src/sfnt/pngshim.h
Copyright: 2013-2024, Google, Inc.
License: FTL

Files: src/sfnt/sfwoff2.c
 src/sfnt/sfwoff2.h
 src/sfnt/woff2tags.c
 src/sfnt/woff2tags.h
Copyright: 2018-2024, Nikhil Ramakrishnan, David Turner, Robert Wilhelm, and Werner Lemberg.
License: FTL

Files: src/sfnt/ttcmapc.h
Copyright: 2009-2024, Oran Agra and Mickey Gabel.
License: FTL

Files: src/sfnt/ttcolr.c
Copyright: 2018-2024, David Turner, Robert Wilhelm, Dominik RÃ¶ttsches, and Werner Lemberg.
License: FTL

Files: src/sfnt/ttgpos.c
 src/sfnt/ttgpos.h
Copyright: 2024, David Saltzman
License: FTL

Files: src/sfnt/ttsbit.c
Copyright: 2013, Google, Inc.
 2005-2024, David Turner, Robert Wilhelm, and Werner Lemberg.
License: FTL

Files: src/sfnt/ttsvg.c
 src/sfnt/ttsvg.h
Copyright: 2022-2024, David Turner, Robert Wilhelm, Werner Lemberg, and Moazin Khatti.
License: FTL

Files: src/svg/*
Copyright: 2022-2024, David Turner, Robert Wilhelm, Werner Lemberg, and Moazin Khatti.
License: FTL

Files: src/tools/apinames.c
Copyright: no-info-found
License: public-domain

Files: src/tools/ftrandom/ftrandom.c
Copyright: 2005, 2007, 2008, 2013, George Williams
License: BSD-3-clause

Files: src/tools/update-copyright-year
Copyright: 2015-2024
License: FTL

Files: src/tools/vms_shorten_symbol.c
Copyright: 2010, 2017, Craig A. Berry
License: Expat or public-domain

Files: src/truetype/ttgxvar.c
Copyright: 2004-2024, David Turner, Robert Wilhelm, Werner Lemberg, and George Williams.
License: FTL

Files: src/truetype/ttgxvar.h
Copyright: 2004-2024, David Turner, Robert Wilhelm, Werner Lemberg and George Williams.
License: FTL

Files: src/type42/*
Copyright: 2002-2024, Roberto Alameda.
License: FTL

Files: src/type42/module.mk
 src/type42/rules.mk
 src/type42/t42error.h
 src/type42/type42.c
Copyright: 1996-2024, David Turner, Robert Wilhelm, and Werner Lemberg.
License: FTL

Files: src/winfonts/winfnt.c
Copyright: 2007, Dmitry Timoshkov for Codeweavers
 2003, Huw D M Davies for Codeweavers
 1996-2024, David Turner, Robert Wilhelm, and Werner Lemberg.
License: FTL

Files: src/winfonts/winfnt.h
Copyright: 2007, Dmitry Timoshkov for Codeweavers
 1996-2024, David Turner, Robert Wilhelm, and Werner Lemberg.
License: FTL

Files: builds/mac/FreeType.m68k_cfm.make.txt builds/mac/FreeType.m68k_far.make.txt docs/DEBUG docs/oldlogs/ChangeLog.21 ft2demos/graph/gblblit.c ft2demos/graph/gblender.c ft2demos/mac/ftoldmac.m68k_far.make.txt ft2demos/src/compos.c ft2demos/src/ftbench.c ft2demos/src/ftchkwd.c ft2demos/src/ftcommon.c ft2demos/src/ftcommon.h ft2demos/src/ftdiff.c ft2demos/src/ftdump.c ft2demos/src/ftgamma.c ft2demos/src/ftgrid.c ft2demos/src/ftlint.c ft2demos/src/ftmulti.c ft2demos/src/ftpatchk.c ft2demos/src/ftpngout.c ft2demos/src/ftsbit.c ft2demos/src/ftstring.c ft2demos/src/fttimer.c ft2demos/src/fttry.c ft2demos/src/ftvalid.c ft2demos/src/ftview.c ft2demos/src/gbench.c ft2demos/src/gbench.h ft2demos/src/output.c ft2demos/src/output.h ft2demos/src/strbuf.c ft2demos/src/strbuf.h ft2demos/src/ttdebug.c ft2docs/docs/design/components.tex ft2docs/docs/design/objects.tex ft2docs/docs/glyphs/outlines.tex src/tools/no-copyright
Copyright: 1996-2024 David Turner, Robert Wilhelm and Werner Lemberg
License: FTL

Files: docs/reference/assets/stylesheets/main.3cba04c6.min.css
Copyright: The FreeType Project
License: FTL

Files: ft2demos/graph/gblender.h
Copyright: 2002-2024 David Turner
License: FTL

Files: ft2demos/graph/graph.h ft2demos/graph/grdevice.h ft2demos/graph/grobjs.h ft2demos/graph/grtypes.h
Copyright: 1999-2024 The FreeType Development Team
License: FTL

Files: ft2demos/mac/ftoldmac.c
Copyright: 1996-2024 Suzuki Toshiya, David Turner, Robert Wilhelm and Werner Lemberg
License: FTL

Files: ft2demos/mac/getargv.c
Copyright: 1991-2024 Stichting Mathematisch Centrum
License: MIT-SMC

Files: ft2demos/src/ftinspect/engine/engine.cpp ft2demos/src/ftinspect/engine/engine.hpp ft2demos/src/ftinspect/ftinspect.cpp ft2demos/src/ftinspect/maingui.cpp ft2demos/src/ftinspect/maingui.hpp ft2demos/src/ftinspect/widgets/customwidgets.cpp ft2demos/src/ftinspect/widgets/customwidgets.hpp
Copyright: 2015-2024 Werner Lemberg
License: FTL

Files: ft2demos/src/ftinspect/glyphcomponents/glyphcontinuous.cpp ft2demos/src/ftinspect/glyphcomponents/glyphcontinuous.hpp ft2demos/src/ftinspect/glyphcomponents/graphicsdefault.cpp ft2demos/src/ftinspect/glyphcomponents/graphicsdefault.hpp
Copyright: 2022-2024 Charlie Jiang
License: FTL

Files: ft2demos/src/ftsdf.c
Copyright: 2021-2024 David Turner, Robert Wilhelm, Werner Lemberg and Anuj Verma
License: FTL

Files: src/gzip/ftzconf.h src/gzip/zutil.h
Copyright: 1995-2023 Jean-loup Gailly and Mark Adler
License: Zlib

Files: src/gzip/zutil.c
Copyright: 1995-2017 Jean-loup Gailly
License: Zlib
